#!/bin/sh

RDF4J_WORKBENCH="${RDF4J_WORKBENCH_HOST}:${RDF4J_WORKBENCH_PORT}"
URL="http://${RDF4J_WORKBENCH}/rdf4j-workbench/repositories"
REPO_ID="test"
FILE="/ta.owl"

if [ "${1}" = "--help" ]; then
	echo "Usage: ${0} [--stamp=stamp-file-to-prevent-repeated-executions] [--repo=new-repo-name] [--file=owl-file-to-load] [--clear]" >&2
	echo "Create a new Halyard RDF4J repository of a particular name (repo='${REPO_ID}' by default), optionally clear its content, and load a particular file (file='${FILE}' by default)." >&2
	echo "After the successful run, optionally create the stamp file to prevent next/repeated executions of the same." >&2
	exit 1
fi

if [ "${1%%=*}" = "--stamp" ]; then
	STAMP_FILE="${1#*=}"
	shift
fi

if [ "${1%%=*}" = "--repo" ]; then
	REPO_ID="${1#*=}"
	shift
fi

if [ "${1%%=*}" = "--file" ]; then
	FILE="${1#*=}"
	shift
fi

if [ -n "${STAMP_FILE}" -a -e "${STAMP_FILE}" ]; then
	echo "*** Cancelling due to the stamp ${STAMP_FILE} ..." >&2
	exit
fi

echo "*** waiting for ${RDF4J_WORKBENCH} ..." >&2
if ! wait-for "${RDF4J_WORKBENCH}"; then
	echo "Timeout!" >&2
	exit 2
fi

echo "*** list repositories ..." >&2
curl -L "${URL}/NONE/repositories"

echo "*** create ${REPO_ID} repository ..." >&2
curl -L "${URL}/NONE/create" -F "type=hbase" -F "Repository ID=${REPO_ID}" -F "Create HBase Table if missing=true" -F "Use Halyard Push Evaluation Strategy=true"

if [ "${1}" = "--clear" ]; then
	echo "*** clear ${REPO_ID} repository ..." >&2
	curl -L "${URL}/${REPO_ID}/clear"
	shift
fi

echo "*** load ${FILE} into the ${REPO_ID} repository ..." >&2
curl -L "${URL}/${REPO_ID}/add" -F "useForContext=on" -F "Content-Type=autodetect" -F "source=file" -F "content=@${FILE}"

if [ -n "${STAMP_FILE}" -a -e "${STAMP_FILE}" ]; then
	echo "*** Creating the stamp ${STAMP_FILE} ..." >&2
	touch "${STAMP_FILE}"
fi
